// Users
{
    "id": 1,
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@mail.com",
    "password": "john1234",
    "isAdmin": false,
    "mobileNo": "09237593671"
}

{
    "id": 2,
    "firstName": "Jane",
    "lastName": "Cruz",
    "email": "janecruz@mail.com",
    "password": "jane12345",
    "isAdmin": false,
    "mobileNo": "09237512341"
}


// Orders
{
    "id": 10,
    "userId": 1,
    "productID" : 25,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 1500
}

{
    "id": 11,
    "userId": 2,
    "productID" : 26,
    "transactionDate": "08-16-2021",
    "status": "paid",
    "total": 2000
}



// Products
{
    "id": 25,
    "name": "Humidifier",
    "description": "Make your home smell fresh any time.",
    "price": 300,
    "stocks": 1286,
    "isActive": true,
}

{
    "id": 26,
    "name": "Fabcon",
    "description": "Make your clothes smell fresh any time.",
    "price": 400,
    "stocks": 1234,
    "isActive": true,
}

